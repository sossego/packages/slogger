/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-console */
import * as Sentry from '@sentry/node';
import axios from 'axios';
import chalk from 'chalk';
import { stringify } from 'flatted/cjs';
import moment from 'moment';
import { v1 as uuid } from 'uuid';

const env = process.env.NODE_ENV;
const sentryDsn = process.env.SENTRY_DSN;

const sloggerHost = process.env.SLOGGER_HOST;
const sloggerAuth = process.env.SLOGGER_AUTH;
const sloggerProject = process.env.SLOGGER_PROJECT;

export enum SloggerLevel {
  CRITICAL = 100,
  ERROR = 200,
  WARNING = 300,
  INFO = 400,
  DEBUG = 500,
}

export enum SloggerEnvironment {
  DEVELOPMENT = 'DEVELOPMENT',
  STAGING = 'STAGING',
  PRODUCTION = 'PRODUCTION',
}

export interface SloggerParams {
  level: SloggerLevel;
  labelColor: string;
  messageColor: string;
  label: string;
  message: string | Error;
  data: any;
  error?: Error;
  stack?: string;
}

export class SloggerService {
  private environment: SloggerEnvironment;

  /**
   * Upon construction starts the one time setup
   */
  public constructor() { this.sloggerSetup(); }

  /**
   * Run small setup during initialization which consists of steps commented below
   */
  private sloggerSetup(): void {

    // Determines environment by matching NODE_ENV (Default: DEVELOPMENT)
    if (env && env.match(/prod|prd/gi)) this.environment = SloggerEnvironment.PRODUCTION;
    else if (env && env.match(/stag|stg|homo/gi)) this.environment = SloggerEnvironment.STAGING;
    else this.environment = SloggerEnvironment.DEVELOPMENT;
    this.info(`Environment configured as ${this.environment}`, { localOnly: true });

    // Sets up Sentry DSN and environment (ignores development)
    if (this.environment !== SloggerEnvironment.DEVELOPMENT && sentryDsn) {
      Sentry.init({
        dsn: sentryDsn,
        environment: this.environment,
        integrations: (ints) => ints.filter((i) => i.name !== 'OnUncaughtException'),
      });
      this.success('Sentry integration ONLINE', { localOnly: true });
    }
    else {
      this.warning('Sentry integration OFFLINE', { localOnly: true });
    }

    // Logs any uncaught exception as error
    process.on('uncaughtException', (err) => {
      this.error(err, { unexpected: true });
    });
  }

  /**
   * Given a logged event by the application, translate its properties into
   * the API standards according to message type being an error or string
   * @param params
   */
  private async log(params: SloggerParams): Promise<void> {
    try {

      // If data is present but is not an object, make it one
      if (params.data && typeof params.data !== 'object') params.data = { data: params.data };

      // If level is error or higher but there is no error object, we must create it.
      // Make sure to remove the top 2 stack traces since they reference this module
      if ((params.level === SloggerLevel.ERROR || params.level === SloggerLevel.CRITICAL) && !(params.message instanceof Error)) {
        params.message = new Error(params.message);
        const stack = params.message.stack.split('\n');
        stack.splice(1, 2);
        params.message.stack = stack.join('\n');
      }

      // Create separate entities for the error object (if present) and the message string
      params.error = params.message instanceof Error ? params.message : undefined;
      params.message = params.message instanceof Error
        ? params.message.message.replace('Error: ', '')
        : params.message;

      // Create separate stringified stack
      params.stack = params.error
        ? `at ${params.error.stack.split(' at ').splice(1).map((str) => str.replace(/\s+/g, ' ').trim()).join('\nat ')}`
        : undefined;

      // Logs the error locally and publish on cloud services
      this.printLog(params);
      await this.publishLog(params);
    }

    // Catch exceptions during the loggin procedure (this should never happen)
    catch (e) {
      e.message = `Slogger: Exception during event logging - ${e.message}`;
      if (this.environment !== SloggerEnvironment.DEVELOPMENT) {
        Sentry.captureException(e);
      }
      console.error(e);
    }
  }

  /**
   * Print messages in the console:
   * - STAGING & PRODUCTION: print as error object and only for WARNING+
   * - DEVELOPMENT: prints as colored string for any level
   * @param params
   */
  private printLog(params: SloggerParams): void {
    const nowStr = moment().format('YYYY-MM-DD HH:mm:ss');

    if (this.environment === SloggerEnvironment.DEVELOPMENT) {
      console.log(chalk`{grey ${nowStr}} {${params.labelColor}  ${params.label} } {${params.messageColor} ${params.message}}`);
      if (params.stack) console.log(chalk`{${params.messageColor} ${params.stack}}`);
      if (params.data && !params.data.localOnly) console.log(params.data);
    }
    else if (params.level <= SloggerLevel.WARNING) {
      console.log(`${nowStr}  ${params.label}  ${params.message}`);
      if (params.error) console.error(params.error);
      if (params.data && !params.data.localOnly) console.log(params.data);
    }
  }

  /**
   * Publish the logs on cloud services obeying following rules:
   * - Ignore DEVELOPMENT environment
   * - ERROR+ on Sentry
   * - INFO+ on Slogger API
   * @param params
   */
  private async publishLog(params: SloggerParams): Promise<void> {
    if (this.environment === SloggerEnvironment.DEVELOPMENT) return undefined;
    if (params.data && params.data.localOnly) return undefined;

    // Creates a matching log id between BigQuery and Sentry
    const id = uuid();

    // If error or worst, sends to Sentry
    if (params.level <= SloggerLevel.ERROR) {
      Sentry.withScope((scope) => {
        scope.setLevel(params.level === SloggerLevel.CRITICAL
          ? Sentry.Severity.Critical
          : Sentry.Severity.Error,
        );
        scope.setTag('id', id);
        if (params.data && params.data.unexpected) scope.setTag('unexpected', 'true');
        scope.setExtras(params.data);
        Sentry.captureException(params.error);
      });
    }

    // Sends to main API which will register it at BigQuery
    if (params.level <= SloggerLevel.INFO) {
      let stringData;
      if (params.data) {
        try { stringData = JSON.stringify(params.data); }
        catch (e) { stringData = stringify(params.data); }
      }

      await axios({
        method: 'POST',
        url: `${sloggerHost}/logs`,
        headers: { 'Authorization': sloggerAuth },
        data: {
          id,
          timestamp: moment().toISOString(),
          project: sloggerProject || 'unknown',
          environment: this.environment,
          level: params.level,
          message: params.message,
          data: stringData,
        },
      });
    }
  }

  /**
   * Level 1: Critical - display as red
   */
  public async critical(message: string | Error, data?: any): Promise<void> {
    await this.log({
      level: SloggerLevel.CRITICAL,
      labelColor: 'white.bgRed',
      messageColor: 'red',
      label: 'CRT',
      message,
      data,
    });
  }

  /**
   * Level 2: Errors - display as red
   */
  public async error(message: string | Error, data?: any): Promise<void> {
    await this.log({
      level: SloggerLevel.ERROR,
      labelColor: 'black.bgRed',
      messageColor: 'red',
      label: 'ERR',
      message,
      data,
    });
  }

  /**
   * Level 3: Warnings - display as yellow
   */
  public async warning(message: string | Error, data?: any): Promise<void> {
    await this.log({
      level: SloggerLevel.WARNING,
      labelColor: 'black.bgYellow',
      messageColor: 'yellow',
      label: 'WRN',
      message,
      data,
    });
  }

  /**
   * Level 4: Information (generic) - display as white
   */
  public async info(message: string | Error, data?: any): Promise<void> {
    await this.log({
      level: SloggerLevel.INFO,
      labelColor: 'black.bgWhite',
      messageColor: 'white',
      label: 'INF',
      message,
      data,
    });
  }

  /**
   * Level 4: Information (success) - display as green
   */
  public async success(message: string | Error, data?: any): Promise<void> {
    await this.log({
      level: SloggerLevel.INFO,
      labelColor: 'black.bgGreen',
      messageColor: 'green',
      label: 'INF',
      message,
      data,
    });
  }

  /**
   * Level 5: Debug - display as grey
   */
  public async debug(message: string | Error, data?: any): Promise<void> {
    await this.log({
      level: SloggerLevel.DEBUG,
      labelColor: 'black.bgBlue',
      messageColor: 'grey',
      label: 'DBG',
      message,
      data,
    });
  }

}

/**
 * Exports as a singleton
 */
export const slogger = new SloggerService();
