# SLOGGER

## Objetivo

Provê um middleware de publicação de logs instalável em qualquer projeto Node.js.

Visa realizar uma integração simples entre os projetos Sossego com os serviços do Sentry e BigQuery.

## Pré-Requisitos

### Não Utilize Imagens Reduzidas do Node.js

Devido a necessidade de instalar este pacote via `git+https`, as imagens reduzidas não irão atender, e o comando de `npm install` irá falhar.

Exemplos:

```
FROM node:12-slim
FROM node:12-stretch
FROM node:14-alpine
```

Utilize:

```
FROM node:12
FROM node:14
```

Isto é válido também para processos de CI/CD, certifique-se que qualquer etapa executando `npm i` está com as imagens completas.


## Instalação

### 1. Crie um Novo Projeto no Sentry

Utilize o link a seguir para criar um novo projeto na plataforma Sentry:

https://sentry.io/organizations/sossego-tech/projects/new/

Preencha os campos da seguinte maneira:
- Em **Choose a platform** escolha `Node.js`
- Em **Set your default alert settings** escolha `Alert me on every new issue`
- Em **Project name** digite o mesmo nome do repositório

Clique em **Create Project**, na tela a seguir o seguinte trecho de código irá aparecer:

```
import * as Sentry from '@sentry/node';
// or using CommonJS
// const Sentry = require('@sentry/node');

Sentry.init({ dsn: 'https://xxx@yyy.ingest.sentry.io/zzz' });
```

Copie a URL do DSN para utilizar no passo a seguir.


### 2. Configure as Variáveis de Ambiente

No arquivo `.env` do projeto, adicione as seguintes variáveis:

```py
# Certifique-se que o env do NodeJS está presente
NODE_ENV='production/staging/development'

# Substitua as variáveis a seguir conforme:
# • Sentry DSN: URL única do Sentry copiada do passo 1
# • Slogger Host: URL da API Slogger usada para integrações
# • Slogger Auth: Valor a ser enviado como Authorization nos Headers
# • Slogger Project: Nome do projeto usado para integrações (via de regra manter idem ao GitLab e Sentry)

SENTRY_DSN='https://xxx@yyy.ingest.sentry.io/zzz'
SLOGGER_HOST='https://slogger-xxx.app'
SLOGGER_AUTH='xyz'
SLOGGER_PROJECT='<nome-do-projeto-git>'
```

### 3. Instale o Pacote Slogger

No diretório do projeto execute o comando:

```
npm i git+https://gitlab.com/sossego/packages/slogger.git
```

## Regras

A aplicação irá extrair o ambiente de execução de acordo com a variável `NODE_ENV`.

Será atribuído um `uuid` gerado automaticamente para cada registro de log, independente da severidade.

O comportamento durante utilização está predefinido para as seguintes regras:

### Desenvolvimento

- Imprime todos os logs no `stdout` e aplica cor na fonte de acordo com o método
- Não realiza integração com a API
- Não realiza integração com o Sentry

### Homologação

- Imprime apenas severidade `WARNING+` e utiliza o `stderr`
- Envia os logs de severidade `INFO+` para a API com a tag de ambiente `STAGING`
- Envia os logs de severidade `ERROR+` para o Sentry com a tag de ambiente `STAGING`

### Produção

- Imprime apenas severidade `WARNING+` e utiliza o `stderr`
- Envia os logs de severidade `INFO+` para a API com a tag de ambiente `PRODUCTION`
- Envia os logs de severidade `ERROR+` para o Sentry com a tag de ambiente `PRODUCTION`



## Utilização

### Importação

Em qualquer módulo do projeto adicione ao topo:

```ts
const { slogger } = require('slogger'); // JavaScript

import { slogger } from 'slogger'; // TypeScript
```

O objeto `slogger` conterá o singleton da aplicação.


### Métodos

Os métodos disponíveis para registro de eventos são em orderm de criticidade:

```ts
log.debug();     // Level 500
log.info();      // Level 400
log.warning();   // Level 300
log.error();     // Level 200
log.critical();  // Level 100
```

Todos os métodos aceitam os mesmo dois parâmetros:

```ts
error(message: string | Error, data?: any): Promise<void>;
```

Parâmetro | Mandatório | Descrição
:-- | :-- | :--
message | Sim | Uma mensagem de texto ou objeto de erro
data | Não | Um objeto qualquer contendo dados relevantes para o log

Embora o parâmetro `message` aceite uma `string`, no caso dos métodos `error` e `critical` ele será automaticamente convertido em um objeto `Error` para podermos usufruir do `stack`.

#### Exemplo

```ts
import { slogger } from 'slogger';

const documento = {
  nome: 'JOSÉ PAULO',
  cpf: '123.456.789-00',
}

try {
  log.debug('Gravando documento...');
  // ...
  log.success('Documento gravado com sucesso!');
}
catch (e) {
  log.error(e, documento);
}
```


### Exceções Não Tratadas

Nem sempre os error estarão tratados em um bloco de `catch` e é possível que ocorram `unhandled exceptions`.

Nestes casos, a aplicação irá automaticamente considerar como um evento de nível `ERROR` e agir de acordo.

Eles serão publicados no Sentry com a tag de `unexpected`.

#### Exemplo

```ts
const pessoa = { nome: 'MARIA' }

const rua = pessoa.endereco.rua; // TypeError: Cannot read property rua of undefined
```

## Boas Práticas

### 1. Não envie o objeto de erro como dado

**Incorreto**
```ts
try {
  // ...
}
catch (e) {
  // Aqui como o 1º param é um string será criado outro objeto erro
  slogger.error('deu erro', e);
}
```

**Correto**
```ts
try {
  // ...
}
catch (e) {
  slogger.error(e);
}
```

### 2. Não faça log de uma string caso tenha o objeto de erro em mãos

**Incorreto**
```ts
const arrayFinal = [ ];

try {
  const array1 = await httpService.get('abc');
  const array2 = await httpService.get('xyz');
  arrayFinal = array1.concat(array2);
}
catch (e) {

  // Ao fazer isto não saberemos se o problema ocorreu ao dar o GET
  // no array1, no array2, ou durante a concatenação
  slogger.error('falha ao concatenar array', arrayFinal);
}
```

**Correto**
```ts
const arrayFinal = [ ];

try {
  const array1 = await httpService.get('abc');
  const array2 = await httpService.get('xyz');
  arrayFinal = array1.concat(array2);
}
catch (e) {
  
  // Se for crucial ter uma mensagem customizada, altere a msg de erro:
  // e.message = `Falha ao concatenar array: ${e.message}`;

  // Se não, simplesmente utilize conforme (em geral isto é suficiente):
  slogger.error(e, arrayFinal);
}
```
