module.exports =  {
  
  // Use the recommended TypeScript parser with default options
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
  },

  // Use recommended rules with type checking
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
  ],

  // Additional plugins
  plugins: [
    'more', // https://github.com/WebbyLab/eslint-plugin-more
    'simple-import-sort', // https://github.com/lydell/eslint-plugin-simple-import-sort
  ],

  // Customizations below considers the above extended rules as starting point
  rules: {
    
    /**
     * DISABLED RECOMMENDED RULES
     */
    '@typescript-eslint/require-await': ['off'], // Forces at least 1 await inside async functions
    '@typescript-eslint/prefer-regexp-exec': ['off'], // Buggy when matching strings
    '@typescript-eslint/no-inferrable-types': ['off'], // Conflicts with typedef

    /**
     * ADDITIONAL ERROR SEVERITY RULES
     */
    'no-throw-literal': ['error'], // No throwing non-error object
    'complexity': ['error', 25 ], // Maximum cyclomatic complexity
    'eqeqeq': ['error'], // Disables == and !=
    'no-dupe-else-if': ['error'], // No duplicates on else if
    'no-import-assign': ['error'], // No assign during imports
    'no-setter-return': ['error'], // No returning on setters
    'more/no-then': ['error'], // No old sintaxe for promises

    /**
     * ADDITIONAL WARNING SEVERITY RULES
     */
    '@typescript-eslint/explicit-member-accessibility': ['warn'], // Must assign properties as public or private
    '@typescript-eslint/indent': ['warn', 2 ], // 2 spaces indent
    '@typescript-eslint/semi': ['warn'], // Semicolons EOL
    '@typescript-eslint/brace-style': ['warn', 'stroustrup', { allowSingleLine: true }], // Line break on logic blocks keywords
    '@typescript-eslint/func-call-spacing': ['warn', 'never'], // No spaces on function calls
    '@typescript-eslint/quotes': ['warn', 'single', { avoidEscape: true }], // Single quotes only
    '@typescript-eslint/typedef': ['warn', { arrowParameter: false }], // Require type definitions except on arrow functions

    'simple-import-sort/sort': ['warn'], // Force import ordering
    'no-console': ['warn'], // No console.log

    'curly': ['warn', 'multi-line', 'consistent'], // Logic brackets must be consistent
    'max-len': ['warn', { 'code': 180, 'comments': 180 }], // Maximum column length

    'no-extra-parens': ['warn'], // No unnecessary parenthesis
    'no-multiple-empty-lines': ['warn', { max: 1 }], // No double empty lines
    'eol-last': ['warn', 'always'], // File must end with line break
    'comma-dangle': ['warn', 'always-multiline'], // Multiline arrays and objects must end with comma

    'no-trailing-spaces': ['warn'], // No unnecessary spaces
    'no-multi-spaces': ['warn'], // No double spaces
    'object-curly-spacing': ['warn', 'always'], // Require spaces on single line objects
    'array-bracket-spacing': ['warn', 'always'], // Require spaces on single line arrays
    'spaced-comment': ['warn', 'always'], // Require spaces on single line commments
    'space-before-blocks': ['warn', 'always'], // Require block spaces
    'comma-spacing': ['warn', { 'before': false, 'after': true }], // Require comma spacing
    'keyword-spacing': ['warn', { before: true, after: true }], // Require keywords spaces
    'space-infix-ops': ['warn'], // Require spaces between math symbols
    
    // Forces camelCase
    '@typescript-eslint/camelcase': ['warn', { allow: [ ] }],

    // Requires JSDoc
    'require-jsdoc': ['warn', {
      'require': {
        'FunctionDeclaration': true,
        'MethodDefinition': true,
        'ClassDeclaration': false,
        'ArrowFunctionExpression': true,
        'FunctionExpression': true,
      }
    }],
    
  },
};
