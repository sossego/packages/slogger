export declare enum SloggerLevel {
    CRITICAL = 100,
    ERROR = 200,
    WARNING = 300,
    INFO = 400,
    DEBUG = 500
}
export declare enum SloggerEnvironment {
    DEVELOPMENT = "DEVELOPMENT",
    STAGING = "STAGING",
    PRODUCTION = "PRODUCTION"
}
export interface SloggerParams {
    level: SloggerLevel;
    labelColor: string;
    messageColor: string;
    label: string;
    message: string | Error;
    data: any;
    error?: Error;
    stack?: string;
}
export declare class SloggerService {
    private environment;
    constructor();
    private sloggerSetup;
    private log;
    private printLog;
    private publishLog;
    critical(message: string | Error, data?: any): Promise<void>;
    error(message: string | Error, data?: any): Promise<void>;
    warning(message: string | Error, data?: any): Promise<void>;
    info(message: string | Error, data?: any): Promise<void>;
    success(message: string | Error, data?: any): Promise<void>;
    debug(message: string | Error, data?: any): Promise<void>;
}
export declare const slogger: SloggerService;
