"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Sentry = __importStar(require("@sentry/node"));
const axios_1 = __importDefault(require("axios"));
const chalk_1 = __importDefault(require("chalk"));
const cjs_1 = require("flatted/cjs");
const moment_1 = __importDefault(require("moment"));
const uuid_1 = require("uuid");
const env = process.env.NODE_ENV;
const sentryDsn = process.env.SENTRY_DSN;
const sloggerHost = process.env.SLOGGER_HOST;
const sloggerAuth = process.env.SLOGGER_AUTH;
const sloggerProject = process.env.SLOGGER_PROJECT;
var SloggerLevel;
(function (SloggerLevel) {
    SloggerLevel[SloggerLevel["CRITICAL"] = 100] = "CRITICAL";
    SloggerLevel[SloggerLevel["ERROR"] = 200] = "ERROR";
    SloggerLevel[SloggerLevel["WARNING"] = 300] = "WARNING";
    SloggerLevel[SloggerLevel["INFO"] = 400] = "INFO";
    SloggerLevel[SloggerLevel["DEBUG"] = 500] = "DEBUG";
})(SloggerLevel = exports.SloggerLevel || (exports.SloggerLevel = {}));
var SloggerEnvironment;
(function (SloggerEnvironment) {
    SloggerEnvironment["DEVELOPMENT"] = "DEVELOPMENT";
    SloggerEnvironment["STAGING"] = "STAGING";
    SloggerEnvironment["PRODUCTION"] = "PRODUCTION";
})(SloggerEnvironment = exports.SloggerEnvironment || (exports.SloggerEnvironment = {}));
class SloggerService {
    constructor() { this.sloggerSetup(); }
    sloggerSetup() {
        if (env && env.match(/prod|prd/gi))
            this.environment = SloggerEnvironment.PRODUCTION;
        else if (env && env.match(/stag|stg|homo/gi))
            this.environment = SloggerEnvironment.STAGING;
        else
            this.environment = SloggerEnvironment.DEVELOPMENT;
        this.info(`Environment configured as ${this.environment}`, { localOnly: true });
        if (this.environment !== SloggerEnvironment.DEVELOPMENT && sentryDsn) {
            Sentry.init({
                dsn: sentryDsn,
                environment: this.environment,
                integrations: (ints) => ints.filter((i) => i.name !== 'OnUncaughtException'),
            });
            this.success('Sentry integration ONLINE', { localOnly: true });
        }
        else {
            this.warning('Sentry integration OFFLINE', { localOnly: true });
        }
        process.on('uncaughtException', (err) => {
            this.error(err, { unexpected: true });
        });
    }
    async log(params) {
        try {
            if (params.data && typeof params.data !== 'object')
                params.data = { data: params.data };
            if ((params.level === SloggerLevel.ERROR || params.level === SloggerLevel.CRITICAL) && !(params.message instanceof Error)) {
                params.message = new Error(params.message);
                const stack = params.message.stack.split('\n');
                stack.splice(1, 2);
                params.message.stack = stack.join('\n');
            }
            params.error = params.message instanceof Error ? params.message : undefined;
            params.message = params.message instanceof Error
                ? params.message.message.replace('Error: ', '')
                : params.message;
            params.stack = params.error
                ? `at ${params.error.stack.split(' at ').splice(1).map((str) => str.replace(/\s+/g, ' ').trim()).join('\nat ')}`
                : undefined;
            this.printLog(params);
            await this.publishLog(params);
        }
        catch (e) {
            e.message = `Slogger: Exception during event logging - ${e.message}`;
            if (this.environment !== SloggerEnvironment.DEVELOPMENT) {
                Sentry.captureException(e);
            }
            console.error(e);
        }
    }
    printLog(params) {
        const nowStr = moment_1.default().format('YYYY-MM-DD HH:mm:ss');
        if (this.environment === SloggerEnvironment.DEVELOPMENT) {
            console.log(chalk_1.default `{grey ${nowStr}} {${params.labelColor}  ${params.label} } {${params.messageColor} ${params.message}}`);
            if (params.stack)
                console.log(chalk_1.default `{${params.messageColor} ${params.stack}}`);
            if (params.data && !params.data.localOnly)
                console.log(params.data);
        }
        else if (params.level <= SloggerLevel.WARNING) {
            console.log(`${nowStr}  ${params.label}  ${params.message}`);
            if (params.error)
                console.error(params.error);
            if (params.data && !params.data.localOnly)
                console.log(params.data);
        }
    }
    async publishLog(params) {
        if (this.environment === SloggerEnvironment.DEVELOPMENT)
            return undefined;
        if (params.data && params.data.localOnly)
            return undefined;
        const id = uuid_1.v1();
        if (params.level <= SloggerLevel.ERROR) {
            Sentry.withScope((scope) => {
                scope.setLevel(params.level === SloggerLevel.CRITICAL
                    ? Sentry.Severity.Critical
                    : Sentry.Severity.Error);
                scope.setTag('id', id);
                if (params.data && params.data.unexpected)
                    scope.setTag('unexpected', 'true');
                scope.setExtras(params.data);
                Sentry.captureException(params.error);
            });
        }
        if (params.level <= SloggerLevel.INFO) {
            let stringData;
            if (params.data) {
                try {
                    stringData = JSON.stringify(params.data);
                }
                catch (e) {
                    stringData = cjs_1.stringify(params.data);
                }
            }
            await axios_1.default({
                method: 'POST',
                url: `${sloggerHost}/logs`,
                headers: { 'Authorization': sloggerAuth },
                data: {
                    id,
                    timestamp: moment_1.default().toISOString(),
                    project: sloggerProject || 'unknown',
                    environment: this.environment,
                    level: params.level,
                    message: params.message,
                    data: stringData,
                },
            });
        }
    }
    async critical(message, data) {
        await this.log({
            level: SloggerLevel.CRITICAL,
            labelColor: 'white.bgRed',
            messageColor: 'red',
            label: 'CRT',
            message,
            data,
        });
    }
    async error(message, data) {
        await this.log({
            level: SloggerLevel.ERROR,
            labelColor: 'black.bgRed',
            messageColor: 'red',
            label: 'ERR',
            message,
            data,
        });
    }
    async warning(message, data) {
        await this.log({
            level: SloggerLevel.WARNING,
            labelColor: 'black.bgYellow',
            messageColor: 'yellow',
            label: 'WRN',
            message,
            data,
        });
    }
    async info(message, data) {
        await this.log({
            level: SloggerLevel.INFO,
            labelColor: 'black.bgWhite',
            messageColor: 'white',
            label: 'INF',
            message,
            data,
        });
    }
    async success(message, data) {
        await this.log({
            level: SloggerLevel.INFO,
            labelColor: 'black.bgGreen',
            messageColor: 'green',
            label: 'INF',
            message,
            data,
        });
    }
    async debug(message, data) {
        await this.log({
            level: SloggerLevel.DEBUG,
            labelColor: 'black.bgBlue',
            messageColor: 'grey',
            label: 'DBG',
            message,
            data,
        });
    }
}
exports.SloggerService = SloggerService;
exports.slogger = new SloggerService();
//# sourceMappingURL=index.js.map